import unittest
from ..dice import Dice

class TestDice(unittest.TestCase):

    def test_4side(self):
        rolls = []
        die = Dice(4)
        for x in range(1,20):
            rolls.append(die.roll())
        self.assertTrue(all( x <= 4 for x in rolls))

    def test_20side(self):
        rolls = []
        die = Dice(20)
        for x in range(1,20):
            rolls.append(die.roll())
        self.assertTrue(all( x <= 20 for x in rolls))

if __name__ == "__main__":
    unittest.main()
