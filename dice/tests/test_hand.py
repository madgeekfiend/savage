import unittest
from dice.hand import Hand

class TestHand(unittest.TestCase):

    def test_roll(self):
        hand = Hand([1,1,1,1,1])
        self.assertEqual(5, hand.roll())

if __name__ == "__main__":
    unittest.main()
