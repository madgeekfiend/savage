from .dice import Dice

class Hand:
    """
    Holds x number of dice and gives the roll on all of them
    """

    def __init__(self, dices):
        """
        Dice is an array of dices
        """
        self.dices = []
        for dice in dices:
            self.dices.append(Dice(dice))

    def roll(self):
        """
        roll all the dice and return sum
        """
        return sum(map(lambda x: x.roll(), self.dices))

if __name__ == "__main__":
    print("Do something really cool with the hand")
    hand = Hand([1,1,1,1,1])
    print("Rolling the hand. Total: {0}".format(hand.roll()))
