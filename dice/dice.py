import random

class Dice:
    """
    Dice class that can take any number of sides. Number of sides is equal
    to value of dice.
    """

    def __init__(self,sides):
        self.sides = sides

    def roll(self):
        return random.randint(1, self.sides)

    def __str__(self):
        return "1d{0}".format(self.sides)


if __name__ == "__main__":
    """
    Quick test of Dice class. Critical path not really a test that's in the
    other class
    """
    print("Testing dice")
    die = Dice(4)
    print("Rolled dice and got a {0} out of {1}".format(die.roll(), die.sides))
