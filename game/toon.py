from .weapon import Weapon
from ..dice.dice import Dice
import random

class Toon:

    def __init__(self, weapon, **kwargs):
        self.ac = kwargs.get('ac', 0)
        self.strength = kwargs.get('str', 0)
        self.name = kwargs.get('name','Anonymous')
        self.class_type = kwargs.get('class_type', 'None')
        self.hit_points = kwargs.get('hp', 1)
        self.weapon = weapon

    def __str__(self):
        return "{0} has equipped (a) {1} is a {2} and has {3} hit points".format(self.name,
        self.weapon.name,self.class_type,self.hit_points)

    def attack(self, target):
        """
        If the weapon is physical STR vs AC
        """
        die = Dice(20)
        roll = die.roll()
        return roll > target.ac

    def give_damage(self, victim):
        damage = self.weapon.damage_die.roll()
        # victim takes damage
        victim.hit_points = victim.hit_points - damage
        return damage

if __name__ == "__main__":
    sword = Weapon(damage_die=8, proficiency_modifier=3, name="Long Sword")
    player = Toon(sword, ac=18,str=16,name="Fleck",class_type="Fighter", hp=16)
    print(player)
    dagger = Weapon(damage_die=4, proficiency_modifier=0, name="Dagger")
    kobold = Toon(dagger, ac=10, str=10, name="Kobold",class_type="Fighter", hp=5)
    print(kobold)
    while(player.hit_points > 0 and kobold.hit_points > 0):
        # Get input
        attack = input("Do you wish to attack? (y/n)?")
        if attack == "y":
            if player.attack(kobold):
                damage = player.give_damage(kobold)
                if damage > 0:
                    print("You hit the {0} with your {1} for {2} damage".format(kobold.name,
                    player.weapon.name, damage))
                else:
                    print("You hit the {0}. But it was too weak to cause any damage.".format(kobold.name))
            else:
                print("You missed!")
        # Kobold attacks all the time
        if kobold.attack(player):
            damage = kobold.give_damage(player)
            if damage > 0:
                print("The {0} hit you with {1} for {2} damage".format(kobold.name,kobold.weapon.name,
                damage))
        else:
            print("The {0} missed!".format(kobold.name))
        # print out problem with stuff
        print(player)
        print(kobold)
        # Check to see if either is dead
        if kobold.hit_points <= 0:
            print("The {0} is dead! You get nothing.".format(kobold.name))
        if player.hit_points <= 0:
            print("You're dead! By a {0}".format(kobold.name))
