from ..dice.dice import Dice

class Weapon:

    def __init__(self, damage_die = 8, proficiency_modifier = 0, **kwargs):
        """
        Weapon class initialize with modifier, damage
        """
        self.damage_die = Dice(damage_die)
        self.proficiency_modifier = proficiency_modifier
        self.name = kwargs.get('name', '')

    def __str__(self):
        return "{0} with {1} damage. Modifier to attacks {2:+.0f}".format(self.name,
        self.damage_die, self.proficiency_modifier)

if __name__ == "__main__":
    # Initialize a weapon
    sword = Weapon(damage_die=8, proficiency_modifier=3, name="Long Sword")
    print(sword)
